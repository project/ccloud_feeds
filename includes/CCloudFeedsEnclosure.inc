<?php

class CCloudFeedsEnclosure extends FeedsEnclosure {
  /**
   * Slightly modify parent function
   */
  public function getFile($destination) {
    $file = NULL;
    if ($this->getValue()) {
      // Prepare destination directory.
      file_prepare_directory($destination, FILE_MODIFY_PERMISSIONS | FILE_CREATE_DIRECTORY);
      // Copy or save file depending on whether it is remote or local.
      if (drupal_realpath($this->getSanitizedUri())) {
        $file           = new stdClass();
        $file->uid      = 0;
        $file->uri      = $this->getSanitizedUri();
        $file->filemime = $this->getMIMEType();
        $file->filename = $this->getSafeFilename();

        if (drupal_dirname($file->uri) !== $destination) {
          $file = file_copy($file, $destination);
        }
        else {
          // If file is not to be copied, check whether file already exists,
          // as file_save() won't do that for us (compare file_copy() and
          // file_save())
          $existing_files = file_load_multiple(array(), array('uri' => $file->uri));
          if (count($existing_files)) {
            $existing = reset($existing_files);
            $file->fid = $existing->fid;
            $file->filename = $existing->filename;
          }
          file_save($file);
        }
      }
      else {
        if (file_uri_target($destination)) {
          $destination = trim($destination, '/') . '/';
        }
        try {
          $filename = $this->getLocalValue();

          if (module_exists('transliteration')) {
            require_once drupal_get_path('module', 'transliteration') . '/transliteration.inc';
            $filename = transliteration_clean_filename($filename);
          }

          // custom: allow change filename
          $filename = ccloud_feeds_remote_file_rename($filename);

          $file = file_save_data($this->getContent(), $destination . $filename);
        }
        catch (Exception $e) {
          watchdog_exception('Feeds', $e, nl2br(check_plain($e)));
        }
      }

      // We couldn't make sense of this enclosure, throw an exception.
      if (!$file) {
        throw new Exception(t('Invalid enclosure %enclosure', array('%enclosure' => $this->getValue())));
      }

      return $file;
    }
  }
}