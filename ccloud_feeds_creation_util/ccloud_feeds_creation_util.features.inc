<?php
/**
 * @file
 * ccloud_feeds_creation_util.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_feeds_creation_util_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_feeds_creation_util_node_info() {
  $items = array(
    'ccloud_template_news' => array(
      'name' => t('模板 - 新闻'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'ccloud_template_news_list' => array(
      'name' => t('模板 - 新闻列表'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
