<?php
/**
 * @file
 * ccloud_feeds_common.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function ccloud_feeds_common_filter_default_formats() {
  $formats = array();

  // Exported format: Web content.
  $formats['web_content'] = array(
    'format' => 'web_content',
    'name' => 'Web content',
    'cache' => 1,
    'status' => 1,
    'weight' => -7,
    'filters' => array(
      'filter_url' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
