<?php
/**
 * @file
 * ccloud_feeds_common.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ccloud_feeds_common_default_rules_configuration() {
  $items = array();
  $items['rules_ccloud_feeds_activate_feed'] = entity_import('rules_config', '{ "rules_ccloud_feeds_activate_feed" : {
      "LABEL" : "CCloud: Activate feed",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "ccloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "feed_info" : "feed_info" } }
                }
              }
            ],
            "DO" : [ { "ccloud_feeds_feed_activate" : { "node" : [ "node" ] } } ],
            "LABEL" : "Activate"
          }
        }
      ]
    }
  }');
  $items['rules_ccloud_feeds_deactivate_feed'] = entity_import('rules_config', '{ "rules_ccloud_feeds_deactivate_feed" : {
      "LABEL" : "CCloud: Deactivate feed",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "ccloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "IF" : [
              { "node_is_of_type" : {
                  "node" : [ "node" ],
                  "type" : { "value" : { "feed_info" : "feed_info" } }
                }
              }
            ],
            "DO" : [ { "ccloud_feeds_feed_deactivate" : { "node" : [ "node" ] } } ],
            "LABEL" : "Deactivate"
          }
        }
      ]
    }
  }');
  $items['rules_ccloud_feeds_import_feed'] = entity_import('rules_config', '{ "rules_ccloud_feeds_import_feed" : {
      "LABEL" : "CCloud: Import feed",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "ccloud_feeds" ],
      "USES VARIABLES" : { "node" : { "label" : "Node", "type" : "node" } },
      "RULES" : [
        { "RULE" : {
            "DO" : [ { "ccloud_feeds_import" : { "node" : [ "node" ] } } ],
            "LABEL" : "Import"
          }
        }
      ]
    }
  }');
  return $items;
}
