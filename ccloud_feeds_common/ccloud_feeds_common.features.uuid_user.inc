<?php
/**
 * @file
 * ccloud_feeds_common.features.uuid_user.inc
 */

/**
 * Implements hook_uuid_features_default_users().
 */
function ccloud_feeds_common_uuid_features_default_users() {
  $users = array();

  $users[] = array(
  'name' => 'Web content',
  'mail' => 'webcontent@createcloud.cn',
  'theme' => '',
  'signature' => '',
  'signature_format' => 'filtered_html',
  'created' => 1445938209,
  'access' => 0,
  'login' => 0,
  'status' => 1,
  'timezone' => 'Asia/Chongqing',
  'language' => 'zh-hans',
  'picture' => NULL,
  'init' => 'nobody@createcloud.cn',
  'data' => FALSE,
  'uuid' => '112f6321-c8d7-4f68-8b81-c4559cd3777a',
  'roles' => array(
    2 => 'authenticated user',
    5 => 'feeds user',
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:UserAccount',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'homepage' => array(
      'predicates' => array(
        0 => 'foaf:page',
      ),
      'type' => 'rel',
    ),
  ),
  'date' => '2015-10-27 17:30:09 +0800',
);
  return $users;
}
