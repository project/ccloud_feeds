<?php
/**
 * @file
 * ccloud_feeds_common.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ccloud_feeds_common_user_default_roles() {
  $roles = array();

  // Exported role: feeds user.
  $roles['feeds user'] = array(
    'name' => 'feeds user',
    'weight' => 3,
  );

  return $roles;
}
