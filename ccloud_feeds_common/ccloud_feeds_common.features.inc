<?php
/**
 * @file
 * ccloud_feeds_common.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ccloud_feeds_common_node_info() {
  $items = array(
    'feed_info' => array(
      'name' => t('导入源信息'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
