<?php
/**
 * @file
 * ccloud_feeds_example.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ccloud_feeds_example_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_firstnews_com_cn';
  $feeds_importer->config = array(
    'name' => '新闻 - 第一新闻网',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
        'request_timeout' => '',
        'auto_scheme' => 'http',
        'accept_invalid_cert' => 0,
        'one_time_source' => 1,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'date_time' => array(
            'name' => 'Date time',
            'value' => 'h1/span[1]',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '0',
          ),
          'source' => array(
            'name' => 'Source',
            'value' => 'h1/span[1]',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'author' => array(
            'name' => 'Author',
            'value' => '',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'content' => array(
            'name' => 'Content',
            'value' => 'div[@class=\'content\']',
            'raw' => 1,
            'inner' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => 'div[@class=\'content\']//img[1]/@src',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '4',
          ),
          'next_feed_source' => array(
            'name' => 'Next feed source',
            'value' => '',
            'raw' => 1,
            'inner' => 0,
            'debug' => 0,
            'weight' => '5',
          ),
          'content_to_remove' => array(
            'name' => 'Content to remove',
            'value' => '',
            'raw' => 1,
            'inner' => 0,
            'debug' => 0,
            'weight' => '6',
          ),
        ),
        'context' => array(
          'value' => 'id(\'Article\')',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsSelfNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '2',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'date_time',
            'target' => 'field_date_time',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'source',
            'target' => 'field_source',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'author',
            'target' => 'field_author',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'content',
            'target' => 'field_content',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'next_feed_source',
            'target' => 'field_next_feed_source',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'content_to_remove',
            'target' => 'Temporary target 1',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'update_non_existent' => 'skip',
        'input_format' => 'web_content',
        'skip_hash_check' => 0,
        'bundle' => 'ccloud_template_news',
        'import_on_create' => 0,
        'insert_new' => '1',
        'content_type' => 'news_firstnews_com_cn',
      ),
    ),
    'content_type' => 'news_firstnews_com_cn',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_firstnews_com_cn'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'news_list_firstnews_com_cn';
  $feeds_importer->config = array(
    'name' => '新闻列表 - 第一新闻网',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'CCloudFeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
        'one_time_source' => FALSE,
        'user_agent' => '',
        'next_source_xpath' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsExHtml',
      'config' => array(
        'use_tidy' => FALSE,
        'sources' => array(
          'title' => array(
            'name' => 'Title',
            'value' => '.',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '1',
          ),
          'feed_source' => array(
            'name' => 'Feed source',
            'value' => '@href',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '2',
          ),
          'image' => array(
            'name' => 'Image',
            'value' => '',
            'raw' => 0,
            'inner' => 0,
            'debug' => 0,
            'weight' => '3',
          ),
        ),
        'context' => array(
          'value' => '//div[@class=\'col-left\']/ul[contains(@class,\'list\')]/li/a',
        ),
        'display_errors' => 0,
        'source_encoding' => array(
          0 => 'auto',
        ),
        'debug_mode' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '2',
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Blank source 1',
            'target' => 'status',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'parent:nid',
            'target' => 'field_feed_node:etid',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'feed_source',
            'target' => 'feeds_source',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'feed_source',
            'target' => 'field_feed_source',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'image',
            'target' => 'field_image:uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'web_content',
        'skip_hash_check' => 0,
        'bundle' => 'news_firstnews_com_cn',
        'insert_new' => '1',
      ),
    ),
    'content_type' => 'news_list_firstnews_com_cn',
    'update' => 0,
    'import_period' => '3600',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );
  $export['news_list_firstnews_com_cn'] = $feeds_importer;

  return $export;
}
