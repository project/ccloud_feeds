<?php
/**
 * @file
 * ccloud_feeds_example.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function ccloud_feeds_example_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'title' => '第一新闻网 - 要闻',
  'log' => '',
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'sticky' => 0,
  'type' => 'feed_info',
  'language' => 'und',
  'created' => 1460104361,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'eb1e8b8f-35ca-4d5d-ada3-bd199d393002',
  'field_feed_info_category' => array(
    'und' => array(
      0 => array(
        'value' => '新闻',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_channel' => array(
    'und' => array(
      0 => array(
        'value' => '第一新闻网',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_node' => array(),
  'field_feed_info_feed_source' => array(
    'und' => array(
      0 => array(
        'value' => 'http://www.firstnews.com.cn/news/yaowen/',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_feed_status' => array(
    'und' => array(
      0 => array(
        'value' => 'disabled',
      ),
    ),
  ),
  'field_feed_info_feed_type' => array(
    'und' => array(
      0 => array(
        'value' => '资讯',
        'format' => NULL,
      ),
    ),
  ),
  'field_feed_info_node_type' => array(
    'und' => array(
      0 => array(
        'value' => 'news_list_firstnews_com_cn',
        'format' => NULL,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'date' => '2016-04-08 16:32:41 +0800',
  'user_uuid' => 'edc5bc66-0f5e-40bc-9fe3-c6d310a97368',
);
  return $nodes;
}
