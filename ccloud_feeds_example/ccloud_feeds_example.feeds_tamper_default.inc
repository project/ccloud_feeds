<?php
/**
 * @file
 * ccloud_feeds_example.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function ccloud_feeds_example_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-author-trim';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'author';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['news_firstnews_com_cn-author-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
    'only_if_empty' => 0,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set value or default value';
  $export['news_firstnews_com_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-content-absolute_url';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'content';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_firstnews_com_cn-content-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-date_time-find_replace_regex';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'date_time';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/([\\s|\\S]+)来源：[\\s|\\S]+评论：[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_firstnews_com_cn-date_time-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-date_time-trim';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'date_time';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Trim';
  $export['news_firstnews_com_cn-date_time-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-source-find_replace_regex';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'find_replace_regex';
  $feeds_tamper->settings = array(
    'find' => '/[\\s|\\S]+来源：([\\s|\\S]+)评论：[\\s|\\S]+/',
    'replace' => '$1',
    'limit' => '',
    'real_limit' => -1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Find replace REGEX';
  $export['news_firstnews_com_cn-source-find_replace_regex'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_firstnews_com_cn-source-trim';
  $feeds_tamper->importer = 'news_firstnews_com_cn';
  $feeds_tamper->source = 'source';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Trim';
  $export['news_firstnews_com_cn-source-trim'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-blank_source_1-default_value';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '0',
    'only_if_empty' => 0,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set value or default value';
  $export['news_list_firstnews_com_cn-blank_source_1-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-image-absolute_url';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'absolute_url';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Make URLs absolute';
  $export['news_list_firstnews_com_cn-image-absolute_url'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-image-ccloud_extract_tag_attribute';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_extract_tag_attribute';
  $feeds_tamper->settings = array(
    'tag' => 'img',
    'attribute' => 'src',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'CCloud: Extract tag attribute';
  $export['news_list_firstnews_com_cn-image-ccloud_extract_tag_attribute'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-image-ccloud_fetch_specified_items';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_fetch_specified_items';
  $feeds_tamper->settings = array(
    'indexes' => '0',
    'return_string' => 1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'CCloud: Fetch specified item(s)';
  $export['news_list_firstnews_com_cn-image-ccloud_fetch_specified_items'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-image-ccloud_restrict_image';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'image';
  $feeds_tamper->plugin_id = 'ccloud_restrict_image';
  $feeds_tamper->settings = array(
    'width_min' => '100',
    'height_min' => '100',
    'width_max' => '',
    'height_max' => '',
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'CCloud: Restrict image attributes';
  $export['news_list_firstnews_com_cn-image-ccloud_restrict_image'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'news_list_firstnews_com_cn-title-trim';
  $feeds_tamper->importer = 'news_list_firstnews_com_cn';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'trim';
  $feeds_tamper->settings = array(
    'mask' => '',
    'side' => 'trim',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Trim';
  $export['news_list_firstnews_com_cn-title-trim'] = $feeds_tamper;

  return $export;
}
