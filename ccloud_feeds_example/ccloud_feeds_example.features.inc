<?php
/**
 * @file
 * ccloud_feeds_example.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ccloud_feeds_example_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}

/**
 * Implements hook_node_info().
 */
function ccloud_feeds_example_node_info() {
  $items = array(
    'news_firstnews_com_cn' => array(
      'name' => t('新闻 - 第一新闻网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
    'news_list_firstnews_com_cn' => array(
      'name' => t('新闻列表 - 第一新闻网'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('标题'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
