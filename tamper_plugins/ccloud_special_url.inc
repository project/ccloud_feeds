<?php

/**
 * @file
 * Special handling for a url.
 */

$plugin = array(
  'form' => 'feeds_tamper_ccloud_special_url_form',
  'callback' => 'feeds_tamper_ccloud_special_url_callback',
  'name' => 'CCloud: Special url',
  'multi' => 'loop',
  'category' => 'HTML',
);

function feeds_tamper_ccloud_special_url_form($importer, $element_key, $settings) {
  $form = array();
  $form['type'] = array(
    '#type' => 'textfield',
    '#title' => t('Type'),
    '#description' => t('Such as: cnki'),
    '#default_value' => isset($settings['type']) ? $settings['type'] : '',
    '#required' => TRUE,
  );
  return $form;
}

function feeds_tamper_ccloud_special_url_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  $type = $settings['type'];

  $options = array(
    'absolute' => TRUE,
    'query' => array('url' => $field),
  );

  $field = url("special_url/$type", $options);
}