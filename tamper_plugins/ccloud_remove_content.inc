<?php

/**
 * @file
 * Remove content of selected sources.
 */

$plugin = array(
  'form'     => 'feeds_tamper_ccloud_remove_content_form',
  'callback' => 'feeds_tamper_ccloud_remove_content_callback',
  'name' => 'CCloud: Remove content of sources',
  'multi' => 'loop',
  'category' => 'HTML',
);

function feeds_tamper_ccloud_remove_content_form($importer, $element_key, $settings) {
  $form = array();

  foreach (feeds_tamper_get_unique_source_list($importer) as $source) {
    $form['source'][$source] = array(
      '#type' => 'checkbox',
      '#title' => $source,
      '#default_value' => isset($settings['source'][$source]) ? $settings['source'][$source] : FALSE,
    );
  }

  return $form;
}

function feeds_tamper_ccloud_remove_content_callback($result, $item_key, $element_key, &$field, $settings, $source) {
  $item = $result->items[$item_key];

  foreach ($settings['source'] as $source => $to_remove) {
    if ($to_remove) {
      $field = str_replace($item[$source], '', $field);
    }
  }
}